#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
vtpub is a Python3 CLI implementation of the VirusTotal Public API.

TODO:
    -- /file/report
        --- Create unit test
        --- Append API key to dot/config files
        --- Handle non-200 status codes
    -- /file/scan
        --- Handle Windows PATHS
    -- /file/rescan

NOTES:
    -- Credit to doomedraven for API key and config file section.
    -- Public API limited to 4 requests per minute.
    -- Public API Responses:
       --- 200 HTTP status code | Returned if a request was correctly
           handled by the server.
           --- The body of the response will usually be a JSON object (except
               for file downloads), with at least 2 properties:
               --- response_code: 1 | Returned if item is present & can be retrieved.
                                  0 | Returned if item isn't present in dataset.
                                 -2 | Returned if item is still queued for analysis.
               --- verbose_msg:  Provides verbose info regarding response_code.
       -- Errors
           --- 204 | Request rate limit exceeded.
           --- 400 | Bad request.
           --- 403 | You don't have enough privileges to make the request.
"""

import sys
import os
import requests
import argparse
from ratelimit import *
from json.decoder import JSONDecodeError
from requests.exceptions import ConnectionError

ONE_MINUTE = 60  # For :decorator: rate_limited
PATHS = {
    0: '~/.vtpub',
    1: '~/vtpub.conf',
    2: '.vtpub',
    3: 'vtpub.conf'
}


"""
[+++++++++++++++++++++++++++++++++++++++++++++++++]
                  Files & API Key
[+++++++++++++++++++++++++++++++++++++++++++++++++]
"""


class VtApi(object):
    """Enables API endpoints to find/create an API key file.

    Retrieves the user's API key from dot/config vtpub files.
        If the files don't exist, create one at a location that
        specified by the user.

    Attributes:
        url (str): Partial VirusTotal API web link (to be completed
                   by each SUBCLASS).
    """

    def __init__(self):
        """Inits VtAPI with 'url' (partial API web link)."""
        self.url = 'https://www.virustotal.com/vtapi/v2{}'
        self.api_file, self.api_key = self.find_api_key()

    @staticmethod
    def create_config(PATHS):
        """Creates a dot/config vtpub file containing API key.

        If no vtpub file (api_file) exist, prompts for the
        location where vtpub file will be created, and the
        user's API key.

        Args:
            PATHS (dict): Possible dot/config file paths.

        Returns:
            api_file (str): Absolute path to the dot/config file.
        """
        print('\n[+] Creating API key path...\n')
        for i in range(4):
            print('\t[{}] {}'.format(i+1, PATHS.get(i)))

        # CONFIG PATH
        while True:
            try:
                option = int(input('\nSelect an option > '))
            except ValueError as e:
                print('invalid option')
                continue

            if 1 >= option <= 4:
                api_file = PATHS.get(option - 1)  # -1 because the option menu is 1-4
                if api_file in PATHS.values():
                    api_file = os.path.expanduser(api_file).strip()
                    break
            else:
                print('invalid option...')

        # API Key
        user_api_key = input('enter API key > ')
        with open(api_file, 'w') as f:
            f.write(user_api_key)

        return api_file

    def find_api_key(self, api_file=False):
        """Returns API key and path to vtpub file.

        If dot/config vtpub file exists, return the absolute
        path of the vtpub file and the API key within the file
        as a tuple.
        If the file doesn't exist, call create_config() to create
        the dot/config vtpub file.

        Args:
            api_file (bool, optional): We assume that the vtpub
                file does not exist. That way, we can make one
                after we check all the possible paths (PATHS).

        Returns:
            tuple: Contains absolute path of vtpub file (api_file) and
                the API key (api_key).
        """
        if not api_file:
            for conf in PATHS.values():
                if os.path.exists(os.path.expanduser(conf)):
                    api_file = os.path.expanduser(conf).strip()
                    break

        # if config doesn't exist
        if not api_file:
            api_file = self.create_config(PATHS)

        with open(api_file) as f:
            api_key = f.read().strip()
            return api_file, api_key


"""
[++++++++++++++++++++++++++++++++++++++++++++++++++++++]
                      /file/report
[++++++++++++++++++++++++++++++++++++++++++++++++++++++]

-- INFO:
   --- Retrieves file scan reports of a hash.
   --- Form Data:
       --- :arg: api_key is the user's personal API key.
       --- :arg: resource can be an MD5, SHA1, or SHA256 hash of a
                 file that you want to get the most recent report of.


-- TEST SAMPLES:
   --- VirusTotal Sample (md5)
       --- response_code 1 (Present in dataset)
       --- 99017f6eebbac24f351415dd410d522d
   --- Penetration Testing: A Hand-On Approach (sha256)
       --- response_code: 1 (Present in dataset)
       --- a5ae0ea7dabe373ca8306fa99599c4cde8d94309104fb07c3aa54bf982c55d13
   --- Kali (sha256)
       --- response_code: 0 (Not present in dataset)
       --- ed88466834ceeba65f426235ec191fb3580f71d50364ac5131daec1bf976b317
"""


class FileReport(VtApi):
    """Implements the /file/report API endpoint.

    Using a user's API key (represented by 'api_key') and
    a resource (md5, sha1, or sha256 file hash; represented
    by 'file_hash'), retrieves and parses the file's scan report.

    Attributes:
        url (str; INHERITED): Partial VirusTotal API web link.
        api_file (str; INHERITED): Absolute path to dot/config file.

        url (str; OVERRIDE): Complete /file/report endpoint
            web link (no parameters).
        file_hash (str): File hash (md5, sha1, sha256) specified as
            a command-line argument.

    Inheritance:
        FileReport is a SUBCLASS of VtApi. It uses url,
            find_api_keys() and create_config().
    """

    def __init__(self):
        """
        Inits FileReport with:
            url (str; INHERITED): Partial VirusTotal API web link.
            api_file (str; INHERITED): Absolute path to dot/config file.
            find_api_key() (INHERITED): Returns API file and key.
            create_config() (INHERITED): Returns newly created API file.

            url (OVERRIDE): Complete /file/report endpoint web link (no
                parameters).
            file_hash: Resource (md5, sha1, sha256)
        """
        super().__init__()
        self.url                    = self.url.format('/file/report')
        self.file_hash              = sys.argv[2]

    def __str__(self):
        """Returns the resource, API key, and /file/report link."""
        return 'resource: {}\napi_key: {}'.format(self.file_hash, self.api_key)

    @rate_limited(4, ONE_MINUTE)
    def file_report(self):
        """Retrieve file scan reports.

        Retrieve a scan report of a file hash (md5, sha1, sha256)
        from the VirusTotal dataset.

        Returns:
            tuple: Contains JSON object of scan (properties),
                response_code property (response_code), and
                the HTTP status code (response).
        """
        params = {'apikey': self.api_key, 'resource': self.file_hash}

        """Series of connection errors if no network connectivity"""
        try:
            response = requests.get(self.url, params=params)
        except ConnectionError as e:
            print('{}: No network connectivity...'.format(e.__class__.__name__))
            sys.exit()

        """JSONDecodeError if invalid API key"""
        try:
            properties = response.json()
        except JSONDecodeError as e:
            print('{}: Invalid API key...'.format(e.__class__.__name__))
            print('\tDeleting invalid API key file...')
            os.remove(self.api_file)
            self.find_api_key()
            sys.exit()

        response_code = properties.get('response_code')

        return properties, response_code, response

    @staticmethod
    def parse_file_report(properties, response_code, response):
        """Parses /file/report API response.

        Parses and outputs scan results, metadata,
        file hashes, and number of total scans and
        positives.
        """
        if response_code == 1:
            # SCANS (scan)
            print('\n\nSCANS')
            for scan, results in properties.get('scans').items():
                print("\t%-25s detected: %-8s version: %-23s result: %-45s update: %s" %
                      (scan, results.get('detected'), results.get('version'),
                       results.get('result'), results.get('update')))
            print()

            # METADATA (response_code, permalink, scan_date, scan_id, verbose_msg)
            print('METADATA')
            print('\tresponse_code:', properties.get('response_code'))
            print('\tscan_date:',     properties.get('scan_date'))
            print('\tscan_id:',       properties.get('scan_id'))
            print('\tresource:',      properties.get('resource'))
            print('\tpermalink:',     properties.get('permalink'))
            print('\tverbose_msg:',   properties.get('verbose_msg'))
            print()

            # HASHES (md5, sha1, sha256)
            print('HASHES')
            print('\tmd5:',    properties.get('md5'))
            print('\tsha1:',   properties.get('sha1'))
            print('\tsha256:', properties.get('sha256'))
            print()

            # REPORT (total, positives)
            print('REPORT')
            print('\ttotal:',     properties.get('total'))
            print('\tpositives:', properties.get('positives'))

            return 0

        elif response_code == 0 or response_code == -2:
            print('response_code: %d | %s...' %
                  (properties.get('response_code'), properties.get('verbose_msg')))

            return 1


"""
[++++++++++++++++++++++++++++++++++++++++++++++++++++]
                      /file/scan
[++++++++++++++++++++++++++++++++++++++++++++++++++++]

-- INFO:
   --- Lets you send a file for scanning with VT.
   --- File size limit is 32MB.
   --- Because files with backslashes can't be uploaded through
         the public API, a temporary file name will be assigned
         ("/" --> "") for upload.
       --- This obviously is a temporary solution but since
             VirusTotal's own web service uploads an alternate
             file name (everything after the backslash,
             inclusive), this will do for now.

-- TEST FILES:
   --- Normal Case
       --- File with no backslash
   --- Edge Case
       --- File with backslash(es)
"""


class FileScan(VtApi):
    """Implements the /file/scan API endpoint.

    Uploads file_name (up to 32MB) to the VirusTotal dataset
    for scanning. Some metadata (scan_id, permalink, resource)
    and hashes are retrieved while the file is queued for analysis.

    Attributes:
        url (str; INHERITED): Partial VirusTotal API web link.
        api_file (str; INHERITED): Absolute path to dot/config file.

        url (str; OVERRIDE): Complete /file/report endpoint web
            link (no parameters).
        api_file  (str; OVERRIDE): Absolute path to dot/config file.
        file_hash (str): File hash (md5, sha1, sha256) specified as
            a command-line argument.

    Inheritance:
        FileScan is a SUBCLASS of VtApi. It uses url,
            api_file, find_api_keys(), and create_config()
    """
    def __init__(self, file_name):
        """
        Inits FileScan with:
            url (str; INHERITED): Partial VirusTotal API web link.
            api_file (str; INHERITED): Absolute path to dot/config file.
            find_api_key() (INHERITED): Returns API file and key.
            create_config() (INHERITED): Returns newly created API file.

            url (OVERRIDE): Complete /file/report endpoint web link.
                (no parameters)
            file_name (str): File to be uploaded for scan.
            temp_file_name (str): file_name with no backslashes.
                A file with backslashes with backslashes won't
                upload so we upload the temp_file_name instead.
            params (dict): Contains API key parameter and value.
                (for POST request)
            files (dict): Contains file parameter and a tuple of
                the filename and a buffered stream of the file.
                (for POST request)
        """
        super().__init__()
        self.url       = self.url.format('/file/scan')
        self.file_name = file_name
        self.params    = {'apikey': self.api_key}
        self.files     = {'file': (self.file_name.replace("\\", ""),
                                   open(self.file_name, 'rb'))}

    def __str__(self):
        """Returns the file name and API key."""
        return 'file_name: {}\napi_key: {}'.format(self.file_name, self.api_key)

    @rate_limited(4, ONE_MINUTE)
    def file_scan(self):
        """Uploads a file for scan.

        Uploads a file to be analyzed. After the file is queued,
        retrieves some metadata and hashes.

        Returns:
            tuple: Contains JSON object of scan (properties),
                response_code property (response_code), and
                a verbose message (verbose_msg)
        """
        response = requests.post(self.url, files=self.files, params=self.params)
        # There seems to be another error throwing this too.
        #   Maybe move JSONDecodeError to an Exception class.
        try:
            properties = response.json()
        except JSONDecodeError as e:
            print('{}: Invalid API key...'.format(e.__class__.__name__))
            print('\tDeleting invalid API key file...')
            os.remove(self.api_file)
            sys.exit()
        response_code = properties.get('response_code')
        verbose_msg   = properties.get('verbose_msg')

        return properties, response_code, verbose_msg

    def parse_file_scan(self):
        """Parses /file/scan API response.

        Parses and outputs response sent after
        uploading a file (metadata and hashes).
        """
        if os.path.exists(self.file_name):
            properties, response_code, verbose_msg = self.file_scan()
        else:
            print("File '%s' does not exist..." % self.file_name)
            sys.exit()

        if response_code == 1:
            # METADATA (response_code, scan_id, resource, permalink, verbose_msg)
            print('METADATA')
            print('\tresponse_code:', properties.get('response_code'))
            print('\tscan_id:', properties.get('scan_id'))
            print('\tresource:', properties.get('resource'))
            print('\tpermalink:',     properties.get('permalink'))
            print('\tverbose_msg:',   properties.get('verbose_msg'))
            print()

            # HASHES (md5, sha1, sha256)
            print('HASHES')
            print('\tmd5:',    properties.get('md5'))
            print('\tsha1:',   properties.get('sha1'))
            print('\tsha256:', properties.get('sha256'))
            print()

        elif response_code == 0 or response_code == 2:
            print('response_code: %d | %s...' % (response_code, verbose_msg))




"""
[+++++++++++++++++++++++++++++++++++++++++++++]
                   Parser
[+++++++++++++++++++++++++++++++++++++++++++++]
"""


def parse_arguments():
    """Create a command-line parser.

    Returns:
        tuple: Contains the parser (parser) and
            the Namespace object containing the
            arguments and their state (args).
    """
    parser = argparse.ArgumentParser(description="Implementation of the VirusTotal Public API.",
                                     formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=40))
    optional = parser._action_groups.pop()  # Moves 'optional' args after 'required' args
    optional.add_argument('-fr', '--file-report', action='store', dest='hash',
                          help='retrieves file scan reports of a hash.')
    optional.add_argument('-fs', '--file-scan', action='store', dest='file',
                          help='uploads a file for scanning (Less than 32MB).')
    parser._action_groups.append(optional)  # Part of hack
    args = parser.parse_args()

    return parser, args


def flags_present(flags):
    """Check if any flags were given.

    Returns:
        bool: True if a flag was set, false otherwise.
    """
    present_flags = list()
    for flag, state in vars(flags).items():
        if state:
            present_flags.append(flag)
    if not present_flags:
        return False
    return True


"""
[+++++++++++++++++++++++++++++++++++++++++++++]
                   Main
[+++++++++++++++++++++++++++++++++++++++++++++]
"""


def main():
    parser, args = parse_arguments()

    if not flags_present(args):
        return parser.print_help()

    if args.hash:
        t = FileReport()
        properties, response_code, response = t.file_report()
        t.parse_file_report(properties, response_code, response)
    elif args.file:
        file_name = args.file
        if os.path.isfile(args.file):
            if os.path.getsize(file_name) < 33554432:
                t = FileScan(file_name)
                t.parse_file_scan()
            else:
                print("File is too large (over 32 MB)")
                sys.exit()
        else:
            print("%s is not a file..." % file_name)
            sys.exit()
    #else:
    #    print('uh oh')


if __name__ == "__main__":
    main()
