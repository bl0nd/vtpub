# vtpub
A Python3 CLI implementation of the VirusTotal Public API.

## Setup
### API File
Prior to using vtpub, you must either:
  1. Set your VirusTotal API key before starting:

      - Command:
        ```
        echo "<your-apikey>" > [file]
        ```
      - Accepted Files:
        - **\~/.vtpub**
        - **.vtpub**
        - **\~/vtpub.conf**
        - **vtpub.conf**

  2. Start without setting API file (Enter API key and API file when prompted).

  ### Dependencies
  Use your favorite package manager to install required dependencies (e.g. pip).
  ```
  pip install -r requirements.txt
  ```


## Usage
```
$ python --version
Python 3.6.4 :: Anaconda, Inc.

$ python vtpub.py -f <file-hash>

[+] Creating API key path...

	[1] ~/.vtpub
	[2] ~/vtpub.conf
	[3] .vtpub
	[4] vtpub.conf

Select an option > 3
Enter API key > ****************************************************************
file hash (md5, sha1, sha256) > 99017f6eebbac24f351415dd410d522d


SCANS

	Bkav       detected: False   version: 1.3.0.9466   result: None         update: 20180122
	Paloalto   detected: True    version: 1.0          result: generic.ml   update: 20180122
	. . .

METADATA
	response_code: 1
	scan_date: 2018-01-22 13:18:07
	scan_id: **************************************************
	resource: 99017f6eebbac24f351415dd410d522d
	permalink: https://www.virustotal.com/file/******/analysis/******/
	verbose_msg: Scan finished, information embedded

HASHES
	md5: 99017f6eebbac24f351415dd410d522d
	sha1: 4d1740485713a2ab3a4f5822a01f645fe8387f92
	sha256: 52d3df0ed60c46f336c131bf2ca454f73bafdc4b04dfa2aea80746f5ba9e6d1c

REPORT
	total: 66
	positives: 52
```
Usage Notes:
- It assumes you don't have a dot/config vtpub file. If you do, it'll start with a prompt for a file hash instead.
- Anything redacted (*) just means a private variable (e.g. API key) or a super long one (e.g. scan_id).
